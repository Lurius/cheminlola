function toggle(name) {
    var x = document.getElementById(name);
    if (x.style.display === "none") {
        x.style.display = "block";
    } else {
        x.style.display = "none";
    }
}

function preview(get, to) {
    send_html = document.getElementById(get).value;
    var regex = /\$titre:([a-zA-Z0-9À-ÿ \.\\\/\-\'\"]*)\$/g;
    if (send_html.match(regex)){
        var output = send_html.match(regex).forEach((element) =>{
            send_html=send_html.replace(element, '<h2>'+element.replace(regex, '$1')+'</h2>');
        });
    }

    var regex = /\$img:([a-zA-Z0-9À-ÿ \.\\\/\-\'\"]*)\|alt:([a-zA-Z0-9À-ÿ \.\\\/\-\'\"]*)\$/g;
    if (send_html.match(regex)){
        var output = send_html.match(regex).forEach((element) =>{
            send_html=send_html.replace(element, '<img src="/pictures/'+element.replace(regex, '$1')+'.png" alt="'+element.replace(regex, '$2')+'">');
        });
    }

    var regex = /\$img:([a-zA-Z0-9À-ÿ \.\\\/\-\'\"]*)\$/g;
    if (send_html.match(regex)){
        var output = send_html.match(regex).forEach((element) =>{
            send_html=send_html.replace(element, '<img src="/pictures/'+element.replace(regex, '$1')+'.png">');
        });
    }

    var regex = /\$g:([a-zA-Z0-9À-ÿ \.\\\/\-\'\"]*)\$/g;
    if (send_html.match(regex)){
        var output = send_html.match(regex).forEach((element) =>{
            send_html=send_html.replace(element, '<strong>'+element.replace(regex, '$1')+'</strong>');
        });
    }

    var regex = /\$i:([a-zA-Z0-9À-ÿ \.\\\/\-\'\"]*)\$/g;
    if (send_html.match(regex)){
        var output = send_html.match(regex).forEach((element) =>{
            send_html=send_html.replace(element, '<em>'+element.replace(regex, '$1')+'</em>');
        });
    }

    var regex = /\$s:([a-zA-Z0-9À-ÿ \.\\\/\-\'\"]*)\$/g;
    if (send_html.match(regex)){
        var output = send_html.match(regex).forEach((element) =>{
            send_html=send_html.replace(element, '<u>'+element.replace(regex, '$1')+'</u>');
        });
    }

    var regex = /\n/g;
    if (send_html.match(regex)){
        var output = send_html.match(regex).forEach((element) =>{
            send_html=send_html.replace(element, '<br>');
        });
    }

    // console.log(send_html);
    document.getElementById(to).innerHTML = send_html;
}