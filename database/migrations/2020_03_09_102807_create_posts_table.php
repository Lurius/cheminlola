<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePostsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('posts', function (Blueprint $table) {
			$table->string('url_title');

			$table->string('picture')->nullable();

			$table->string('title');
			$table->text('resume');
			$table->text('article');

			$table->boolean('type')->comment('0 - Article d\'évènement | 1 - Article de presse');

			$table->unsignedInteger('user_id')->nullable();

			$table->timestamps();

			$table->primary('url_title');
            $table->foreign('user_id')->references('id')->on('users');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('posts');
	}
}
