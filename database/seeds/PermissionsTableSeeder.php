<?php

use Illuminate\Database\Seeder;
use App\Model\Permission;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions = [
            ['user_right', 'gérer les utilisateur'],
            ['post_right', 'gérer les actualités'], 
            ['image_right', 'gérer les images'],
            ['admin_right', 'modifier les données des pages'],
        ];

        foreach($permissions as $permission) {
            Permission::create([
                'name' => $permission[0],
                'description' => $permission[1],
            ]);
        }
    }
}
