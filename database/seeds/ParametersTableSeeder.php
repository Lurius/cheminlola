<?php

use Illuminate\Database\Seeder;
use App\Model\Parameter;

class ParametersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $parameters = [
            [
                'google',
                'Référencement',
                'Ceci est le texte de présentation pour google si la page n\'est pas un article'
            ],
            [
                'Biographie',
                'Découvrez le combat de Lola',
                'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Pellentesque habitant morbi tristique senectus et netus. Morbi tincidunt ornare massa eget egestas purus viverra. Egestas erat imperdiet sed euismod nisi porta lorem mollis aliquam. Blandit cursus risus at ultrices mi tempus. Congue mauris rhoncus aenean vel elit scelerisque mauris. Ac odio tempor orci dapibus ultrices in iaculis nunc sed. Porttitor leo a diam sollicitudin tempor id eu nisl. In pellentesque massa placerat duis ultricies lacus sed turpis. Amet mauris commodo quis imperdiet massa. Massa eget egestas purus viverra accumsan in. Nisi quis eleifend quam adipiscing vitae proin sagittis nisl rhoncus. Quis ipsum suspendisse ultrices gravida dictum fusce ut. Sed viverra tellus in hac habitasse platea dictumst vestibulum. Purus gravida quis blandit turpis. Amet venenatis urna cursus eget nunc scelerisque viverra. Tempus quam pellentesque nec nam. Pellentesque diam volutpat commodo sed egestas. Sed egestas egestas fringilla phasellus.

                Commodo quis imperdiet massa tincidunt nunc pulvinar sapien. Odio euismod lacinia at quis. Non quam lacus suspendisse faucibus. Vel risus commodo viverra maecenas. Nulla pellentesque dignissim enim sit. Nunc mattis enim ut tellus elementum sagittis vitae et. Et ligula ullamcorper malesuada proin libero nunc consequat interdum varius. Interdum velit euismod in pellentesque massa placerat duis ultricies. Varius quam quisque id diam vel quam elementum pulvinar etiam. Nisi porta lorem mollis aliquam ut porttitor leo a diam. In nulla posuere sollicitudin aliquam ultrices sagittis orci a scelerisque. Tortor id aliquet lectus proin nibh nisl condimentum id venenatis. Nunc vel risus commodo viverra maecenas accumsan lacus vel facilisis. Suspendisse potenti nullam ac tortor vitae. Pharetra et ultrices neque ornare aenean. Mollis nunc sed id semper risus in hendrerit.
                
                At augue eget arcu dictum varius duis. Quis hendrerit dolor magna eget. Vestibulum morbi blandit cursus risus. Nulla aliquet enim tortor at. Et tortor at risus viverra adipiscing at. Sit amet mattis vulputate enim nulla aliquet porttitor. In arcu cursus euismod quis. Pellentesque elit eget gravida cum sociis natoque. Integer quis auctor elit sed vulputate. Gravida neque convallis a cras semper auctor neque vitae. Vitae purus faucibus ornare suspendisse sed nisi lacus. Cras semper auctor neque vitae tempus quam. Justo donec enim diam vulputate ut pharetra sit. Fermentum odio eu feugiat pretium nibh ipsum consequat. Leo vel orci porta non pulvinar neque.
                
                Et malesuada fames ac turpis egestas sed tempus urna et. Aliquam etiam erat velit scelerisque in dictum non. Magna fringilla urna porttitor rhoncus dolor purus non enim praesent. Purus non enim praesent elementum facilisis leo. Posuere morbi leo urna molestie at elementum eu facilisis sed. Mauris commodo quis imperdiet massa tincidunt nunc pulvinar sapien. Nibh mauris cursus mattis molestie. Sit amet nisl purus in mollis. Ultrices in iaculis nunc sed augue lacus viverra vitae congue. Diam sollicitudin tempor id eu nisl. Consequat id porta nibh venenatis. Nec dui nunc mattis enim ut tellus.
                
                Quam viverra orci sagittis eu volutpat odio facilisis mauris. Ultrices neque ornare aenean euismod elementum. Aliquam purus sit amet luctus. Interdum posuere lorem ipsum dolor sit. Arcu vitae elementum curabitur vitae nunc sed velit dignissim sodales. Eu facilisis sed odio morbi quis commodo odio aenean. Placerat vestibulum lectus mauris ultrices eros in cursus turpis. Vel turpis nunc eget lorem dolor sed. At tempor commodo ullamcorper a lacus. Gravida in fermentum et sollicitudin ac orci. Enim praesent elementum facilisis leo vel fringilla est ullamcorper. At ultrices mi tempus imperdiet nulla malesuada. Tempor nec feugiat nisl pretium fusce. Suspendisse sed nisi lacus sed viverra tellus in hac habitasse. Erat pellentesque adipiscing commodo elit. Tempor id eu nisl nunc mi ipsum faucibus vitae aliquet.',
            ],
            [
                'Biographie',
                'En savoir plus sur l\'AVC infantile...',
                'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Pellentesque habitant morbi tristique senectus et netus. Morbi tincidunt ornare massa eget egestas purus viverra. Egestas erat imperdiet sed euismod nisi porta lorem mollis aliquam. Blandit cursus risus at ultrices mi tempus. Congue mauris rhoncus aenean vel elit scelerisque mauris. Ac odio tempor orci dapibus ultrices in iaculis nunc sed. Porttitor leo a diam sollicitudin tempor id eu nisl. In pellentesque massa placerat duis ultricies lacus sed turpis. Amet mauris commodo quis imperdiet massa. Massa eget egestas purus viverra accumsan in. Nisi quis eleifend quam adipiscing vitae proin sagittis nisl rhoncus. Quis ipsum suspendisse ultrices gravida dictum fusce ut. Sed viverra tellus in hac habitasse platea dictumst vestibulum. Purus gravida quis blandit turpis. Amet venenatis urna cursus eget nunc scelerisque viverra. Tempus quam pellentesque nec nam. Pellentesque diam volutpat commodo sed egestas. Sed egestas egestas fringilla phasellus.

                Commodo quis imperdiet massa tincidunt nunc pulvinar sapien. Odio euismod lacinia at quis. Non quam lacus suspendisse faucibus. Vel risus commodo viverra maecenas. Nulla pellentesque dignissim enim sit. Nunc mattis enim ut tellus elementum sagittis vitae et. Et ligula ullamcorper malesuada proin libero nunc consequat interdum varius. Interdum velit euismod in pellentesque massa placerat duis ultricies. Varius quam quisque id diam vel quam elementum pulvinar etiam. Nisi porta lorem mollis aliquam ut porttitor leo a diam. In nulla posuere sollicitudin aliquam ultrices sagittis orci a scelerisque. Tortor id aliquet lectus proin nibh nisl condimentum id venenatis. Nunc vel risus commodo viverra maecenas accumsan lacus vel facilisis. Suspendisse potenti nullam ac tortor vitae. Pharetra et ultrices neque ornare aenean. Mollis nunc sed id semper risus in hendrerit.
                
                At augue eget arcu dictum varius duis. Quis hendrerit dolor magna eget. Vestibulum morbi blandit cursus risus. Nulla aliquet enim tortor at. Et tortor at risus viverra adipiscing at. Sit amet mattis vulputate enim nulla aliquet porttitor. In arcu cursus euismod quis. Pellentesque elit eget gravida cum sociis natoque. Integer quis auctor elit sed vulputate. Gravida neque convallis a cras semper auctor neque vitae. Vitae purus faucibus ornare suspendisse sed nisi lacus. Cras semper auctor neque vitae tempus quam. Justo donec enim diam vulputate ut pharetra sit. Fermentum odio eu feugiat pretium nibh ipsum consequat. Leo vel orci porta non pulvinar neque.
                
                Et malesuada fames ac turpis egestas sed tempus urna et. Aliquam etiam erat velit scelerisque in dictum non. Magna fringilla urna porttitor rhoncus dolor purus non enim praesent. Purus non enim praesent elementum facilisis leo. Posuere morbi leo urna molestie at elementum eu facilisis sed. Mauris commodo quis imperdiet massa tincidunt nunc pulvinar sapien. Nibh mauris cursus mattis molestie. Sit amet nisl purus in mollis. Ultrices in iaculis nunc sed augue lacus viverra vitae congue. Diam sollicitudin tempor id eu nisl. Consequat id porta nibh venenatis. Nec dui nunc mattis enim ut tellus.
                
                Quam viverra orci sagittis eu volutpat odio facilisis mauris. Ultrices neque ornare aenean euismod elementum. Aliquam purus sit amet luctus. Interdum posuere lorem ipsum dolor sit. Arcu vitae elementum curabitur vitae nunc sed velit dignissim sodales. Eu facilisis sed odio morbi quis commodo odio aenean. Placerat vestibulum lectus mauris ultrices eros in cursus turpis. Vel turpis nunc eget lorem dolor sed. At tempor commodo ullamcorper a lacus. Gravida in fermentum et sollicitudin ac orci. Enim praesent elementum facilisis leo vel fringilla est ullamcorper. At ultrices mi tempus imperdiet nulla malesuada. Tempor nec feugiat nisl pretium fusce. Suspendisse sed nisi lacus sed viverra tellus in hac habitasse. Erat pellentesque adipiscing commodo elit. Tempor id eu nisl nunc mi ipsum faucibus vitae aliquet.',
            ],
            [
                'Actualités',
                'Agenda',
                'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Pellentesque habitant morbi tristique senectus et netus. Morbi tincidunt ornare massa eget egestas purus viverra. Egestas erat imperdiet sed euismod nisi porta lorem mollis aliquam. Blandit cursus risus at ultrices mi tempus. Congue mauris rhoncus aenean vel elit scelerisque mauris. Ac odio tempor orci dapibus ultrices in iaculis nunc sed. Porttitor leo a diam sollicitudin tempor id eu nisl. In pellentesque massa placerat duis ultricies lacus sed turpis. Amet mauris commodo quis imperdiet massa. Massa eget egestas purus viverra accumsan in. Nisi quis eleifend quam adipiscing vitae proin sagittis nisl rhoncus. Quis ipsum suspendisse ultrices gravida dictum fusce ut. Sed viverra tellus in hac habitasse platea dictumst vestibulum. Purus gravida quis blandit turpis. Amet venenatis urna cursus eget nunc scelerisque viverra. Tempus quam pellentesque nec nam. Pellentesque diam volutpat commodo sed egestas. Sed egestas egestas fringilla phasellus.

                Commodo quis imperdiet massa tincidunt nunc pulvinar sapien. Odio euismod lacinia at quis. Non quam lacus suspendisse faucibus. Vel risus commodo viverra maecenas. Nulla pellentesque dignissim enim sit. Nunc mattis enim ut tellus elementum sagittis vitae et. Et ligula ullamcorper malesuada proin libero nunc consequat interdum varius. Interdum velit euismod in pellentesque massa placerat duis ultricies. Varius quam quisque id diam vel quam elementum pulvinar etiam. Nisi porta lorem mollis aliquam ut porttitor leo a diam. In nulla posuere sollicitudin aliquam ultrices sagittis orci a scelerisque. Tortor id aliquet lectus proin nibh nisl condimentum id venenatis. Nunc vel risus commodo viverra maecenas accumsan lacus vel facilisis. Suspendisse potenti nullam ac tortor vitae. Pharetra et ultrices neque ornare aenean. Mollis nunc sed id semper risus in hendrerit.
                
                At augue eget arcu dictum varius duis. Quis hendrerit dolor magna eget. Vestibulum morbi blandit cursus risus. Nulla aliquet enim tortor at. Et tortor at risus viverra adipiscing at. Sit amet mattis vulputate enim nulla aliquet porttitor. In arcu cursus euismod quis. Pellentesque elit eget gravida cum sociis natoque. Integer quis auctor elit sed vulputate. Gravida neque convallis a cras semper auctor neque vitae. Vitae purus faucibus ornare suspendisse sed nisi lacus. Cras semper auctor neque vitae tempus quam. Justo donec enim diam vulputate ut pharetra sit. Fermentum odio eu feugiat pretium nibh ipsum consequat. Leo vel orci porta non pulvinar neque.
                
                Et malesuada fames ac turpis egestas sed tempus urna et. Aliquam etiam erat velit scelerisque in dictum non. Magna fringilla urna porttitor rhoncus dolor purus non enim praesent. Purus non enim praesent elementum facilisis leo. Posuere morbi leo urna molestie at elementum eu facilisis sed. Mauris commodo quis imperdiet massa tincidunt nunc pulvinar sapien. Nibh mauris cursus mattis molestie. Sit amet nisl purus in mollis. Ultrices in iaculis nunc sed augue lacus viverra vitae congue. Diam sollicitudin tempor id eu nisl. Consequat id porta nibh venenatis. Nec dui nunc mattis enim ut tellus.
                
                Quam viverra orci sagittis eu volutpat odio facilisis mauris. Ultrices neque ornare aenean euismod elementum. Aliquam purus sit amet luctus. Interdum posuere lorem ipsum dolor sit. Arcu vitae elementum curabitur vitae nunc sed velit dignissim sodales. Eu facilisis sed odio morbi quis commodo odio aenean. Placerat vestibulum lectus mauris ultrices eros in cursus turpis. Vel turpis nunc eget lorem dolor sed. At tempor commodo ullamcorper a lacus. Gravida in fermentum et sollicitudin ac orci. Enim praesent elementum facilisis leo vel fringilla est ullamcorper. At ultrices mi tempus imperdiet nulla malesuada. Tempor nec feugiat nisl pretium fusce. Suspendisse sed nisi lacus sed viverra tellus in hac habitasse. Erat pellentesque adipiscing commodo elit. Tempor id eu nisl nunc mi ipsum faucibus vitae aliquet.',
            ],
            [
                'Contact',
                'Téléphone',
                '06 08 98 50 37',
            ],
            [
                'Contact',
                'E-mail',
                '...@cheminlola.asso.st',
            ],
            [
                'Contact',
                'Comment nous aider ?',
                'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Pellentesque habitant morbi tristique senectus et netus. Morbi tincidunt ornare massa eget egestas purus viverra. Egestas erat imperdiet sed euismod nisi porta lorem mollis aliquam. Blandit cursus risus at ultrices mi tempus. Congue mauris rhoncus aenean vel elit scelerisque mauris. Ac odio tempor orci dapibus ultrices in iaculis nunc sed. Porttitor leo a diam sollicitudin tempor id eu nisl. In pellentesque massa placerat duis ultricies lacus sed turpis. Amet mauris commodo quis imperdiet massa. Massa eget egestas purus viverra accumsan in. Nisi quis eleifend quam adipiscing vitae proin sagittis nisl rhoncus. Quis ipsum suspendisse ultrices gravida dictum fusce ut. Sed viverra tellus in hac habitasse platea dictumst vestibulum. Purus gravida quis blandit turpis. Amet venenatis urna cursus eget nunc scelerisque viverra. Tempus quam pellentesque nec nam. Pellentesque diam volutpat commodo sed egestas. Sed egestas egestas fringilla phasellus.

                Commodo quis imperdiet massa tincidunt nunc pulvinar sapien. Odio euismod lacinia at quis. Non quam lacus suspendisse faucibus. Vel risus commodo viverra maecenas. Nulla pellentesque dignissim enim sit. Nunc mattis enim ut tellus elementum sagittis vitae et. Et ligula ullamcorper malesuada proin libero nunc consequat interdum varius. Interdum velit euismod in pellentesque massa placerat duis ultricies. Varius quam quisque id diam vel quam elementum pulvinar etiam. Nisi porta lorem mollis aliquam ut porttitor leo a diam. In nulla posuere sollicitudin aliquam ultrices sagittis orci a scelerisque. Tortor id aliquet lectus proin nibh nisl condimentum id venenatis. Nunc vel risus commodo viverra maecenas accumsan lacus vel facilisis. Suspendisse potenti nullam ac tortor vitae. Pharetra et ultrices neque ornare aenean. Mollis nunc sed id semper risus in hendrerit.
                
                At augue eget arcu dictum varius duis. Quis hendrerit dolor magna eget. Vestibulum morbi blandit cursus risus. Nulla aliquet enim tortor at. Et tortor at risus viverra adipiscing at. Sit amet mattis vulputate enim nulla aliquet porttitor. In arcu cursus euismod quis. Pellentesque elit eget gravida cum sociis natoque. Integer quis auctor elit sed vulputate. Gravida neque convallis a cras semper auctor neque vitae. Vitae purus faucibus ornare suspendisse sed nisi lacus. Cras semper auctor neque vitae tempus quam. Justo donec enim diam vulputate ut pharetra sit. Fermentum odio eu feugiat pretium nibh ipsum consequat. Leo vel orci porta non pulvinar neque.
                
                Et malesuada fames ac turpis egestas sed tempus urna et. Aliquam etiam erat velit scelerisque in dictum non. Magna fringilla urna porttitor rhoncus dolor purus non enim praesent. Purus non enim praesent elementum facilisis leo. Posuere morbi leo urna molestie at elementum eu facilisis sed. Mauris commodo quis imperdiet massa tincidunt nunc pulvinar sapien. Nibh mauris cursus mattis molestie. Sit amet nisl purus in mollis. Ultrices in iaculis nunc sed augue lacus viverra vitae congue. Diam sollicitudin tempor id eu nisl. Consequat id porta nibh venenatis. Nec dui nunc mattis enim ut tellus.
                
                Quam viverra orci sagittis eu volutpat odio facilisis mauris. Ultrices neque ornare aenean euismod elementum. Aliquam purus sit amet luctus. Interdum posuere lorem ipsum dolor sit. Arcu vitae elementum curabitur vitae nunc sed velit dignissim sodales. Eu facilisis sed odio morbi quis commodo odio aenean. Placerat vestibulum lectus mauris ultrices eros in cursus turpis. Vel turpis nunc eget lorem dolor sed. At tempor commodo ullamcorper a lacus. Gravida in fermentum et sollicitudin ac orci. Enim praesent elementum facilisis leo vel fringilla est ullamcorper. At ultrices mi tempus imperdiet nulla malesuada. Tempor nec feugiat nisl pretium fusce. Suspendisse sed nisi lacus sed viverra tellus in hac habitasse. Erat pellentesque adipiscing commodo elit. Tempor id eu nisl nunc mi ipsum faucibus vitae aliquet.',
            ],
            [
                'Legal',
                'Mentions légales',
                'Demander à Fantine son Texte'

            ],
            [
                'Envie de nous rejoindre',
                '',
                'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Pellentesque habitant morbi tristique senectus et netus. Morbi tincidunt ornare massa eget egestas purus viverra. Egestas erat imperdiet sed euismod nisi porta lorem mollis aliquam. Blandit cursus risus at ultrices mi tempus. Congue mauris rhoncus aenean vel elit scelerisque mauris. Ac odio tempor orci dapibus ultrices in iaculis nunc sed. Porttitor leo a diam sollicitudin tempor id eu nisl. In pellentesque massa placerat duis ultricies lacus sed turpis. Amet mauris commodo quis imperdiet massa. Massa eget egestas purus viverra accumsan in. Nisi quis eleifend quam adipiscing vitae proin sagittis nisl rhoncus. Quis ipsum suspendisse ultrices gravida dictum fusce ut. Sed viverra tellus in hac habitasse platea dictumst vestibulum. Purus gravida quis blandit turpis. Amet venenatis urna cursus eget nunc scelerisque viverra. Tempus quam pellentesque nec nam. Pellentesque diam volutpat commodo sed egestas. Sed egestas egestas fringilla phasellus.

                Commodo quis imperdiet massa tincidunt nunc pulvinar sapien. Odio euismod lacinia at quis. Non quam lacus suspendisse faucibus. Vel risus commodo viverra maecenas. Nulla pellentesque dignissim enim sit. Nunc mattis enim ut tellus elementum sagittis vitae et. Et ligula ullamcorper malesuada proin libero nunc consequat interdum varius. Interdum velit euismod in pellentesque massa placerat duis ultricies. Varius quam quisque id diam vel quam elementum pulvinar etiam. Nisi porta lorem mollis aliquam ut porttitor leo a diam. In nulla posuere sollicitudin aliquam ultrices sagittis orci a scelerisque. Tortor id aliquet lectus proin nibh nisl condimentum id venenatis. Nunc vel risus commodo viverra maecenas accumsan lacus vel facilisis. Suspendisse potenti nullam ac tortor vitae. Pharetra et ultrices neque ornare aenean. Mollis nunc sed id semper risus in hendrerit.
                
                At augue eget arcu dictum varius duis. Quis hendrerit dolor magna eget. Vestibulum morbi blandit cursus risus. Nulla aliquet enim tortor at. Et tortor at risus viverra adipiscing at. Sit amet mattis vulputate enim nulla aliquet porttitor. In arcu cursus euismod quis. Pellentesque elit eget gravida cum sociis natoque. Integer quis auctor elit sed vulputate. Gravida neque convallis a cras semper auctor neque vitae. Vitae purus faucibus ornare suspendisse sed nisi lacus. Cras semper auctor neque vitae tempus quam. Justo donec enim diam vulputate ut pharetra sit. Fermentum odio eu feugiat pretium nibh ipsum consequat. Leo vel orci porta non pulvinar neque.
                
                Et malesuada fames ac turpis egestas sed tempus urna et. Aliquam etiam erat velit scelerisque in dictum non. Magna fringilla urna porttitor rhoncus dolor purus non enim praesent. Purus non enim praesent elementum facilisis leo. Posuere morbi leo urna molestie at elementum eu facilisis sed. Mauris commodo quis imperdiet massa tincidunt nunc pulvinar sapien. Nibh mauris cursus mattis molestie. Sit amet nisl purus in mollis. Ultrices in iaculis nunc sed augue lacus viverra vitae congue. Diam sollicitudin tempor id eu nisl. Consequat id porta nibh venenatis. Nec dui nunc mattis enim ut tellus.
                
                Quam viverra orci sagittis eu volutpat odio facilisis mauris. Ultrices neque ornare aenean euismod elementum. Aliquam purus sit amet luctus. Interdum posuere lorem ipsum dolor sit. Arcu vitae elementum curabitur vitae nunc sed velit dignissim sodales. Eu facilisis sed odio morbi quis commodo odio aenean. Placerat vestibulum lectus mauris ultrices eros in cursus turpis. Vel turpis nunc eget lorem dolor sed. At tempor commodo ullamcorper a lacus. Gravida in fermentum et sollicitudin ac orci. Enim praesent elementum facilisis leo vel fringilla est ullamcorper. At ultrices mi tempus imperdiet nulla malesuada. Tempor nec feugiat nisl pretium fusce. Suspendisse sed nisi lacus sed viverra tellus in hac habitasse. Erat pellentesque adipiscing commodo elit. Tempor id eu nisl nunc mi ipsum faucibus vitae aliquet.',
            ],
            [
                'Footer',
                '',
                'Association Le chemin de Lola
                18 rue Pasteur 42110 Feurs'
            ]
        ];

        foreach ($parameters as $parameter) {
            Parameter::create([
                'section'   => $parameter[0],
                'title'     => $parameter[1],
                'content'   => $parameter[2],
            ]);
        }
    }
}
