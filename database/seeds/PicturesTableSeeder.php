<?php

use Illuminate\Database\Seeder;
use App\Model\Image;

class PicturesTableSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$images = [
			[
				'banniere_large',
				'Bannière du chemin de Lola sur grand et moyen écran',
			],
			[
				'banniere_small',
				'Bannière du chemin de Lola sur petit écran',
			],
			[
				'icon',
				'Logo du chemin de Lola',
			],
			[
				'facebook',
				'Logo facebook pour le liens dans la bannière',
			],
			[
				'twitter',
				'Logo twitter pour le liens dans la bannière',
			],
			[
				'carousel1',
				'1° image carousel'
			],
			[
				'carousel2',
				'2° image carousel'
			],
			[
				'carousel3',
				'3° image carousel'
			],
			[
				'carousel4',
				'4° image carousel'
			],
			[
				'lien-evenement-image',
				'Apparait comme fond au lien vers la page actualité'
			],
			[
				'lien-membre-image',
				'Apparait comme fond au lien vers la page membres'
			],
			[
				'lien-presse-image',
				'Apparait comme fond au lien vers la page presse'
			],
		];

		foreach ($images as $image) {
			Image::create([
				'name' 			=> $image[0],
				'description' 	=> $image[1],
				'type' 			=> 0,
			]);
		}
	}
}
