<?php

use Illuminate\Database\Seeder;
use App\Model\User;
use App\Model\Permission;
use App\Model\PermissionUser;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = [
            "a@a.a",
            "admin temp",
            "$2y$12\$urbQSRCxFU0wqSTnXIe5se6UTiNuBReQaT/YOaRMNRqolU6lTYQXa",
        ];

        User::create([
            "email" => $user[0],
            "name"  => $user[1],
            "password" => $user[2],
        ]);

        $id = User::where("email", $user[0])->first()->id;

        $permissions = Permission::all();

        foreach ($permissions as $permission) {
            PermissionUser::create([
                "permission_id" => $permission->id,
                "user_id"       => $id,
            ]);
        }
    }
}
