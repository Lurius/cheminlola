<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Page d'accueil
Route::get('/','IndexController@index')->name('accueil');

// L'association
// Qui sommes-nous ? 
Route::get('/qui-sommes-nous', 'IndexController@bio')->name('bio');
// Nos membres (Bloc "Découvrez nos membres" Galerie photo avec carousel en plein écran quand clic sur une photo / (à la fin de tous) (Mise en fond article (Titre plus bass et centrer + Bouton centrer aussi : "Envie de nous rejoindre" (Ouvre texte (mise en forme article) qui contient le param "Devenir membre"))))
Route::get('/membres', 'IndexController@member')->name('member');

// Actualités
// Agenda (Bloc "Agenda" -> Zone de texte ||| Bouton "Découvrez nos derniers évenements" + Voir Plus (sur un fond image (diff)))
Route::get('/agenda', 'IndexController@agenda')->name('agenda');
// Nos évènements (Bloc "Nos évenement" ->  Titre centré : "Nos dernières actualités... (casé recherche + Bouton non présent sur maquette) -> Chaque article (Mise en forme article) avec une image ||| Bouton "La presse en parle" + Voir plus (sur un fond image (didiff)))
Route::get('/actualites', 'PostController@index')->name('actualite');
// La presse en parle (Bloc "La presse en parle" -> (Mise en forme article sans titre mais avec image à chaque) chaque article)
Route::get('/presse', 'PresseController@index')->name('presse');

// Galerie Photo (Galerie photo avec carousel en pleine écran quand clic sur une photo | Quelque part faut que j'arrive à limiter le nombre d'image pour coller au modèle (pas prio))
Route::get('/galerie', 'IndexController@galery')->name('galerie');

// Liste des soutiens (Bloc "Nos soutiens" -> Liste d'image des soutiens | "Nos parrains" (titre centrer en dehors mise en forme) (Mise en forme article (sans texte mais 2 image)) Liste image parrains)
Route::get('/soutien', 'IndexController@soutien')->name('soutien');

// Contact/Nous aider (Bloc toutes longueur maj "Nous contacter / nous aider" -> Logo téléphone + param "téléphone" / meme pour e-mail  |/ (Mise en forme article) "Comment nous aider ?" param)
Route::get('/contact', 'IndexController@contact')->name('contact');

// Mentions Légale (dans footer) (Bloc "Mentions légales" -> Texte)
Route::get('/mention-legale', 'IndexController@mention')->name('mention');

// Connexion
Route::post('/connexion', 'IndexController@connexion')->name('connexion');
Route::get('/logout', 'IndexController@logout')->name('logout');

// Vérifie que l'utilisateur est connecté (vérife droit en dessous)
Route::middleware(['perms:connected'])->group(function() {
    // Page gestion compte
    Route::get('/compte', 'UserController@compte')->name('utilisateur.compte');
    Route::post('/utilisateur/update/{id}','UserController@update')->name('utilisateur.update');
    Route::post('/utilisateur/delete/{id}','UserController@destroy')->name('utilisateur.delete');

    // Vérifie droit de gérer utilisateur
    Route::middleware(['perms:user_right'])->group(function(){
        Route::get('/utilisateurs', 'UserController@index')->name('utilisateur.index');
        Route::post('/utilisateur/store','UserController@store')->name('utilisateur.store');
        Route::get('/utilisateur/{id}', 'UserController@show')->name('utilisateur.show');

        Route::post('/permissions/utilisateur/{id}', 'PermissionController@update')->name('perm.user');
    });

    // Vérifie droit de gérer post
    Route::middleware(['perms:post_right'])->group(function() {
        Route::get('/actualite/nouveau', 'PostController@create')->name('article.create');
        Route::post('/actualite/store','PostController@store')->name('article.store');
        Route::get('/actualite/modifier/{url_title}', 'PostController@edit')->name('article.edit');
        Route::post('/actualite/update/{url_title}','PostController@update')->name('article.update');
        Route::post('/actualite/delete/{url_title}','PostController@destroy')->name('article.delete');

        Route::get('/presse/nouveau', 'PresseController@create')->name('presse.create');
        Route::post('/presse/store','PresseController@store')->name('presse.store');
        Route::get('/presse/modifier/{url_title}', 'PresseController@edit')->name('presse.edit');
        Route::post('/presse/update/{url_title}','PresseController@update')->name('presse.update');
        Route::post('/presse/delete/{url_title}','PresseController@destroy')->name('presse.delete');
    });

    // Vérifie droit de gérer images
    Route::middleware(['perms:image_right'])->group(function() {
        Route::get('/images', 'ImageController@index')->name('image.index');
        Route::get('/images/nouveau', 'ImageController@create')->name('image.create');
        Route::post('/images/store','ImageController@store')->name('image.store');
        Route::get('/images/{id}', 'ImageController@show')->name('image.show');
        Route::post('/images/delete/{id}','ImageController@destroy')->name('image.delete');
    });

    // Vérifie droit de modifié soutien
    Route::middleware(['perms:admin_right'])->group(function() {
        Route::get('/admin', 'IndexController@admin')->name('admin');
        Route::post('/admin/update/{id}', 'IndexController@up_admin')->name('admin.update');
        // Page pour changer les variables des pages principales
    });
});

// Affiche un post précis
Route::get('/actualite/{url_title}', 'PostController@show')->name('article');
Route::get('/presse/{url_title}', 'PostController@show')->name('lien');