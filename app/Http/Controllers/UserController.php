<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use App\Model\User;
use App\Model\Permission;
use App\Model\PermissionUser;
use App\Model\Post;

class UserController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		$users = User::with('user_rights')->get();
		$perms = Permission::all();
		return view ('users.index', compact(['users', 'perms']));
	}

	/**
	 * Display the logged user info.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function compte() {
		$user = User::find(session()->get('user'));

		return view('users.show', compact('user'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		$validator = Validator::make($request->all(),
			[
				'email'     =>  'required|email',
				'password'   =>  'required'
			]
		);

		if($validator->fails()) {
            return back()->withErrors($validator->errors());
        }

		// Si l'adresse mail n'existe pas déjà, alors on créé l'utilisateur
		if(!User::where('email', $request->get('email'))->first()){
			User::create([
				'email' => $request->get('email'),
				'name' => $request->get('name'),
				'password' => Hash::make($request->get('password'))
			]);
		}

		return back();

	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		$user = User::find($id);
		$is_admin =  true;

		return view('users.show', compact(['user', 'is_admin']));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		// Regarde si c'est une modification depuis la page admin ou perso et vérifie alors si le mot de passe actuel est mis
		if($request->get("is_admin")) {
			$validator = Validator::make($request->all(), 
				[
					'current_email'     =>  'required|email',
					'current_name'		=>	'required',
				]
			);
		} else {
			$validator = Validator::make($request->all(),
				[
					'current_email'     =>  'required|email',
					'current_name'		=>	'required',
					'current_pass'		=>	'required',
				]
			);
		}

		if($validator->fails()) {
            return back()->withErrors($validator->errors());
        }

		// Regarde si la demande concerne aussi une modification de mot de passe
		if($request->get("new_pass")){
			$validator = Validator::make($request->all(),
				[
					'new_pass'		=> 'required',
					'new_pass_redo' => 'required',
				]
			);
		}
		if($validator->fails()) {
            return back()->withErrors($validator->errors());
		}
		// Si le mot de passe ne sont pas identique retourne une erreur
		if($request->get("new_pass") <> $request->get("new_pass_redo")){
			return back()->withErrors("Mot de passe non identique");
		}

		$user = User::find($id);
		if(!$request->get("is_admin")) {
			// Vérifie que la mot de passe est le bon avant de continuer
			if(!Hash::check($request->get('current_pass'), $user->password)){
				return back();
			}
		}
		$user->update([
			'email' => $request->get("current_email"),
			'name'	=> $request->get("current_name"),
		]);
		// Modifi le mot de passe si un nouveau à été mis
		if($request->get("new_pass")){
			$user->update([
				'password' => Hash::make($request->get('new_pass')),
			]);
		}
		$user->save();

		return back();
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		// On efface toutes les permissions de l'utilisateur
		$perms = PermissionUser::where("user_id", $id)->delete();
		// On nullifie l'identifiant utilisateur si il a écrit des articles pour actualités
		$post = Post::where("user_id", $id)->update(["user_id" => null]);
		// Enfin on l'efface
		$user = User::find($id)->delete();

		return back();
	}
}
