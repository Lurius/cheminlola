<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Permission;
use App\Model\PermissionUser;

class PermissionController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		// Récupère toutes les permissions stocké
		$perms = Permission::all();

		foreach($perms as $perm){
			// Si bouton coché et que la ligne n'existe pas déjà, alors on créé la ligne
			if($request->get($perm->name) && !PermissionUser::where("permission_id", '=', $perm->id)->where("user_id", '=', $id)->first()) {
				PermissionUser::create([
					"permission_id" => $perm->id,
					"user_id" => $id,
				]);
			// Si le bouton n'était pas coché et que la ligne existe, on efface la ligne
			} elseif(!$request->get($perm->name) && PermissionUser::where("permission_id", '=', $perm->id)->where("user_id", '=', $id)->first()) {
				PermissionUser::where("permission_id", '=', $perm->id)->where("user_id", '=', $id)->delete();
			}
		}

		return back();
	}
}
