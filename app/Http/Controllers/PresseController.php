<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Model\Post;
use App\Model\Permission;
use App\Model\PermissionUser;
use App\Model\Parameter;

class PresseController extends Controller
{
	/**
	 * Fonction visant à changer un titre en une url propre
	 * 
	 * @param String $title
	 * @return String $title
	 */
	public function urlify(String $title)
	{
		$title = str_replace(" l'", " ", $title);
		$title = str_replace(" n'", " ", $title);
		$title = str_replace(" le ", " ", $title);
		$title = str_replace(" la ", " ", $title);
		$title = str_replace(" les ", " ", $title);
		$title = str_replace(" me ", " ", $title);
		$title = str_replace(" ma ", " ", $title);
		$title = str_replace(" mes ", " ", $title);
		$title = str_replace(" de ", " ", $title);
		$title = str_replace(" des ", " ", $title);
		$title = str_replace(" à ", " ", $title);

		$title = str_replace("L'", "", $title);
		$title = str_replace("N'", "", $title);
		$title = str_replace("Le ", "", $title);
		$title = str_replace("La ", "", $title);
		$title = str_replace("Les ", "", $title);
		$title = str_replace("Me ", "", $title);
		$title = str_replace("Ma ", "", $title);
		$title = str_replace("Mes ", "", $title);
		$title = str_replace("De ", "", $title);
		$title = str_replace("Des ", "", $title);
		$title = str_replace("À ", "", $title);

		$title = str_replace("?", "", $title);
		$title = str_replace("!", "", $title);
		$title = str_replace(".", "", $title);
		$title = str_replace("/", "", $title);
		$title = str_replace("\\", "", $title);

		while(strpos($title, "  ")>0){
			$title = str_replace("  ", " ", $title);
		}

		$title = str_replace(" ", "-", $title);


		return strtolower($title);
	}


	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		if(session()->has("user")) {
			$perm_id = Permission::where("name","post_right")->first()->id;
			if(PermissionUser::where("user_id", session()->get("user"))->where("permission_id", $perm_id)->first())
			{
				$admin = true;
			} else {
				$admin = false;
			}
		} else {
			$admin = false;
		}

		if(isset($_GET["search"]) && $_GET["search"] <> ""){
			$posts = Post::where("type", 1)->where(function ($q) { $q->where("title", "%".$_GET["search"]."%")->orWhere("resume", "%".$_GET["search"]."%"); })->get();
		}else {
			$posts = Post::where("type", 1)->get();
		}

		return view('presse.index', compact(['posts', 'admin']));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		return view('presse.new');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		$validator      =   Validator::make($request->all(),
			[
				'picture'	=>	'required',
				'title'     =>  'required',
				'resume'	=>	'required',
				'article'   =>  'required',
				'type'		=> 	'required',
			]
		);
		// if validation fails
		if($validator->fails()) {
			return back()->withErrors($validator->errors());
		}

		$url_title = $this->urlify($request->get("title"));

		if(Post::where("url_title", $url_title)->first()){
			return back()->withErrors("Titre déjà existant ou trop proche");
		}

		Post::create([
			'url_title'	=> $url_title,
			'picture'	=> $request->get("picture"),
			'title'		=> $request->get("title"),
			'resume'	=> $request->get("resume"),
			'article'	=> $request->get("article"),
			'user_id'	=> session()->get("user"),
			'type'		=> $request->get("type"),
		]);

		return back();
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  String  $url_title
	 * @return \Illuminate\Http\Response
	 */
	public function show($url_title)
	{
		if(session()->has("user")) {
			$perm_id = Permission::where("name","post_right")->first()->id;
			if(PermissionUser::where("user_id", session()->get("user"))->where("permission_id", $perm_id)->first())
			{
				$admin = true;
			} else {
				$admin = false;
			}
		} else {
			$admin = false;
		}

		$post = Post::where('url_title',$url_title)->first();

		return view('presse.show', compact(['post', 'admin']));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  String  $url_title
	 * @return \Illuminate\Http\Response
	 */
	public function edit($url_title)
	{
		$post = Post::where('url_title',$url_title)->first();

		return view('presse.update', compact('post'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  String  $url_title
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $url_title)
	{
		$post = Post::where('url_title',$url_title)->first();
		
		$validator = Validator::make($request->all(),
		[
			'picture'	=>	'required',
			'title'     =>  'required',
			'resume'	=>	'required',
			'article'   =>  'required',
			'type'		=> 	'required',
		]
		);
		// if validation fails
		if($validator->fails()) {
			return back()->withErrors($validator->errors());
		}
		
		$url_title = $this->urlify($request->get("title"));
		
		if($post->url_title <> $url_title){
			if(Post::where("url_title", $url_title)->first()){
				return back()->withErrors("Titre déjà existant ou trop proche");
			}
		}
		
		$post->update([
			'url_title'	=> $url_title,
			'picture'	=> $request->get("picture"),
			'title'		=> $request->get("title"),
			'resume'	=> $request->get("resume"),
			'article'	=> $request->get("article"),
			'type'		=> $request->get("type"),
		]);

		return redirect()->route('presse.edit', [$url_title]);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  String  $url_title
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($url_title)
	{
		$post = Post::where('url_title',$url_title)->first()->delete();

		return redirect()->route('actualite');
	}
}
