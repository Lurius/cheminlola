<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\File;
use App\Model\Image;

class ImageController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		// fetch all the images
		if(isset($_GET["type"]) && $_GET["type"] <> ""){
			$images =	Image::where("type", $_GET["type"])->get();
		}else {
			$images	=	Image::all();
		}
		 return view('images.index', compact('images'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		return view('images.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		// file validation
		$validator      =   Validator::make($request->all(),
			[
				'filename'     =>	'required|mimes:jpeg,png,jpg,bmp|max:2048',
				'name'         =>	'required',
				'type'		   =>	'required',
			]
		);


		// if validation fails
		if($validator->fails()) {
			return back()->withErrors($validator->errors());
		}

		// if validation success
		if($images   =   $request->file('filename')) {

			$name      =   $request->get('name').'.png';

			$target_path    =   public_path('/pictures/');
			
			if($images->move($target_path, $name)) {

				// save file name in the database
				$images   =   Image::create(['name'=> $request->get('name'), 'description' => $request->get('description')??'', 'type' => $request->get('type')]);

				return back()->with("success", "Image enregistrer");
			}
		}

		return back();
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		$image = Image::find($id);

		return view('images.show', compact('image'));
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		$image = Image::find($id);
		$file = public_path('/pictures/').$image->name.".png";
		File::delete($file);

		$image->delete();

		return redirect()->route('image.index');
	}
}
