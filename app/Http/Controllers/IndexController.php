<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use App\Model\User;
use App\Model\Post;
use App\Model\Parameter;
use App\Model\Image;

class IndexController extends Controller
{
	/**
	 * Display the page for all of the data use under
	 * 
	 * @return \Illuminate\Http\Response
	 */
	public function admin() {
		$parameters = Parameter::all();

		return view('admin.index', compact('parameters'));
	}

	/**
	 * Store all modification
	 * 
	 * @param \Illuminate\Http\Request  $request
	 * @param Int $id
	 * @return \Illuminate\Http\Response
	 */
	public function up_admin(Request $request, $id) {
		$param = Parameter::find($id);
		$param->update([
			'title'		=>	$request->get('title')??' ',
			'content'	=>	$request->get('content')??' ',
		]);

		return back();
	}	


	/**
	 * Page d'accueil du site avec un carousel (gérer via gestion des images (les noms des images)),
	 * le dernier évènement ajouter et quelques image de soutiens.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		$last_event = Post::where("type", 0)->first();
		$soutiens = Image::where("type", 3)->get()->take(8); //(Limité à 8/6)
		
		return view('index', compact('last_event', 'soutiens'));
	}

	/**
	 * Page de présentation de l'association et de la pathologie de Lola + Un liens pour découvrir les membres
	 * (Bloc "Qui sommes-nous ?" 
	 * -> "Découvrez le combat de Lola ..." + Texte 
	 * / (Mise en forme article) "En savoir plus sur l'AVC infantile  
	 * ||| Bouton "Découvrez nos membres" + Voir Plus (sur un fond image))
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function bio()
	{
		$info = Parameter::where("section", "Qui sommes nous ?")->get();

		return view('bio', compact('info'));
	}

	/**
	 * Page qui affiche la liste des photos des membres
	 * 
	 * @return \Illuminate\Http\Response
	 */
	public function member()
	{
		$pictures = Image::where("type", 1)->get();
		$join = Parameter::where("section", "Envie de nous rejoindre")->first();

		return view('member', compact(['pictures', 'join']));
	}

	/**
	 * Page contenant un texte précisant les évènements à venir
	 * 
	 * @return \Illuminate\Http\Response
	 */
	public function agenda()
	{
		$info = Parameter::where("section", "Actualités")->where("title", "Agenda")->first();

		return view('agenda', compact('info'));
	}

	/**
	 * Page qui affiche une sélection d'image
	 * 
	 * @return \Illuminate\Http\Response
	 */
	public function galery()
	{
		$pictures = Image::where("type", 2)->get();

		return view('galery', compact('pictures'));
	}

	/**
	 * Page qui affiche la liste des logos de soutiens, suivi de la liste des parrains
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function soutien()
	{
		$soutiens = Image::where("type", 3)->get();
		$parrains = Image::where("type", 4)->get();

		return view('supporter', compact(['soutiens', 'parrains']));
	}

	/**
	 * Page donnant le numéro de téléphone et l'adresse mail de contact ainsi qu'un encart pour préciser comment aider l'association
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function contact()
	{
		$contact = Parameter::where("section", "Contact")->get();

		return view('contact', compact('contact'));
	}

	/**
	 * Page accessible depuis footer et contenant les mentions légales du site
	 * 
	 * @return \Illuminate\Http\Response
	 */
	public function mention()
	{
		$info = Parameter::where("section", "Legal")->first();
	
		return view('legal', compact('info'));
	}

	/**
	 * If informations correct, log in user
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function connexion(Request $request)
	{
		$validator      =   Validator::make($request->all(),
			[
				'email'     =>  'required|email',
				'password'   =>  'required'
			]
		);
		// if validation fails
		if($validator->fails()) {
			return back()->withErrors($validator->errors());
		}

		$user = User::where("email", $request->get("email"))->first();
		if($user){
			if(Hash::check($request->get("password"), $user->password)){
				session(['user' => $user->id]);
			}
		}

		return back();
	}

	public function logout() {
		session()->forget('user');
		
		return back();
	}
}
