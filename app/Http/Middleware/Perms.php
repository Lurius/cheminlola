<?php

namespace App\Http\Middleware;

use Closure;
use App\Model\User;
use App\Model\Permission;

class Perms
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $target)
    {
        if($target == "connected") {
            if(session()->has("user")){
                return $next($request);
            }
        }
        if($target == "user_right" || $target == "post_right" || $target == "image_right" || $target == "admin_right"){
            $perm_id = Permission::where("name", $target)->first()->id;

            $user = User::with("user_rights")->find(session()->get("user"));

            if($user->user_rights) {
                $continu = false;
                foreach($user->user_rights as $right){
                    if($right->permission_id == $perm_id){
                        $continu = true;
                    }
                }
            }
            if($continu){
                return $next($request);
            }
        }
        
        return redirect()->route('accueil');

    }
}
