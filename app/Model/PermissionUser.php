<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class PermissionUser extends Model
{
    // Timestamps deleted in migration
    public $timestamps = false;
    
    protected $table = 'permission_user';

    protected $fillable = [
        "permission_id",
        "user_id",
    ];
}
