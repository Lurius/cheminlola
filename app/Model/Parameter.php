<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Parameter extends Model
{
    // Timestamps deleted in migration
    public $timestamps = false;

    protected $fillable = [
        'section',
        'title',
        'content'
    ];
}
