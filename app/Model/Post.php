<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Post extends Model
{
    public $primaryKey = "url_title";
    public $incrementing = false;

    // In Laravel 6.0+ make sure to also set $keyType
    protected $keyType = 'string';
    
    protected $fillable = [
        'url_title',
        'picture',
        'title',
        'resume',
        'article',
        'user_id',
        'type',
    ];

    public function writer(): BelongsTo
    {
        return $this->belongsTo('App\Model\User');
    }
}
