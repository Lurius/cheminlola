<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
    // Timestamps deleted in migration
    public $timestamps = false;

    protected $fillable = [
        'name',
        'description',
    ];
}
