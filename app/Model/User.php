<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;

class User extends Model
{
    protected $fillable = [
        'email',
        'name',
        'password'
    ];

    
    public function user_posts(): HasMany
    {
        return $this->hasMany('App\Model\Post');
    }

    public function user_rights(): HasMany
    {
        return $this->hasMany('App\Model\PermissionUser');
    }
}
