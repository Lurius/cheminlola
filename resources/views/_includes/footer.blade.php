<footer class="footer row">
	<div class="col-md-5 col-sm-12 row ml-15">
		<a href="javascript:void(0);" class="w-100 footer-link @if(Request::url() == route('bio') || Request::url() == route('member')) actived @endif" onclick="$('#asso-foot').toggleClass('collapse');">L'association</a>
		<div id="asso-foot" class="collapse">
			<div class="w-100">
				<a class="footer-link sub @if(Request::url() == route('bio')) actived @endif" href="{{ route('bio') }}">Qui sommes-nous ?</a>
			</div>
			<div class="w-100">
				<a class="footer-link sub @if(Request::url() == route('member')) actived @endif" href="{{ route('member') }}">Nos membres</a>
			</div>
		</div>
		<a class="footer-link w-100" href="{{ route('mention') }}">Mentions légales</a>
		<?php 
		use App\Model\Parameter;
		$adresse =  Parameter::where("section", "Footer")->first();

		echo "<p class='w-100 footer-text'>$adresse->content</p>";
		?>
	</div>
	<div class="col-md-3 col-sm-12 footer-button">
		<a class="see-more footer-button" href="{{ route('contact') }}">Nous contacter</a>
	</div>
	<div class="col-md-4 col-sm-12">
		<a href="https://www.facebook.com/associationlechemindelola/" target="_blank"><img class="logo-social" src="/system/facebook.svg"></a>
		<a href="https://twitter.com/lechemindelola" target="_blank"><img class="logo-social twitter" src="/system/twitter.svg"></a>
	</div>
</footer>
{{-- Le footer bleu ciel (récup #4ce), Contient de gauche à droite
	1 box contenant tous les liens du menu mais les uns au dessus des autre sans le liens contact (devra donc aussi pour faire apparaitre les sous liens pour assoc et actu)
	1 gros bouton rose centrer : "Nous contacter" renvoyant sans doute sur page contact 
	Les 2 logo facebook et twitter en arrondi amenant sur page--}}

