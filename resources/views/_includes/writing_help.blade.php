<div class="w-100">
	<div class="box">
		<button class="w-100 btn btn-success" onclick="toggle('help')">Aide écrit</button>
		<div id="help" style="display:none">
			<table class="table table-dark">
				<thead>
					<tr>
						<th>Propriété</th>
						<th>Explication</th>
						<th>Résultat</th>
					</tr>
				</thead>
				<tbody style="text-transform:none">
					<tr>
						<td>$titre:Chemin de Lola$</td> 
						<td>Transforme le texte en un titre</td>
						<td><h2>Chemin de Lola</h2></td>
					</tr>
					<tr>
						<td>$img:icon|alt:logo Chemin de Lola$</td> 
						<td>Affiche une image grâce à son nom en y mettant la description indiqué</td>
						<td><img src="/pictures/icon.png" alt="logo Chemin de Lola"></td>
					</tr>
					<tr>
						<td>$img:icon$</td> 
						<td>Affiche une image grâce à son nom sans mettre de description (non recommandé)</td>
						<td><img src="/pictures/icon.png"></td>
					</tr>
					<tr>
						<td>$g:Chemin de Lola$</td> 
						<td>Met le texte en gras (ne fonctionne pas avec $s:...$ et $i:...$)</td>
						<td><strong>Chemin de Lola</strong></td>
					</tr>
					<tr>
						<td>$s:Chemin de Lola$</td> 
						<td>Souligne le texte (ne fonctionne pas avec $g:...$ et $i:...$)</td>
						<td><u>Chemin de Lola</u></td>
					</tr>
					<tr>
						<td>$i:Chemin de Lola$</td> 
						<td>Met le texte en italique (ne fonctionne pas avec $g:...$ et $s:...$)</td>
						<td><em>Chemin de Lola</em></td>
					</tr>
					<tr>
						<td></td>
						<td>L'HTML/CSS/JS est possible en cas de manque de mécanique ou de besoin particulier</td>
						<td></td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</div>