<div class="content">
<a href="{{ route('accueil') }}">
	<img class="banniere_large" src="/pictures/banniere_large.png">
	<img class="banniere_medium" src="/pictures/banniere_medium.png">
	<img class="banniere_small" src="/pictures/banniere_small.png">
</a>
</div>
<nav>
	@if(session()->has('user'))
	{{-- Mettre un menu pour chaque page en fonction permission --}}
	<button class="show-medium" onclick="$('#menu_admin').toggleClass('collapse-medium');">=</button>
	<div id="menu_admin" class="w-100 row menu-medium collapse-medium">
		<div class="col-lg-4 col-md-nav-12">
			<a class="nav-link @if(Request::url() == route('admin')) actived @endif" href="{{ route('admin') }}">Gestion des pages</a>
		</div>
		<div class="col-lg-4 col-md-nav-12">
			<a class="nav-link @if(Request::url() == route('image.index')) actived @endif" href="{{ route('image.index') }}">Gestion des images</a>
		</div>
		<div class="col-lg-4 col-md-nav-12">
			<a class="nav-link @if(Request::url() == route('utilisateur.index')) actived @endif" href="{{ route('utilisateur.index') }}">Gestion des utilisateurs</a>
		</div>
	</div>
	@endif
	<button class="show-medium" onclick="$('#menu').toggleClass('collapse-medium');">=</button>
	<div id="menu" class="w-100 row menu-medium collapse-medium" style="justify-content:center">
		<div class="col-lg-2 col-md-nav-12">
			<a href="javascript:void(0);" class="nav-link @if(Request::url() == route('bio') || Request::url() == route('member')) actived @endif" onclick="$('#asso-list').toggleClass('collapse');">L'association</a>
			<div id="asso-list" class="collapse">
				<div class="w-100">
					<a class="nav-link sub @if(Request::url() == route('bio')) actived @endif" href="{{ route('bio') }}">Qui sommes-nous ?</a>
				</div>
				<div class="w-100">
					<a class="nav-link sub @if(Request::url() == route('member')) actived @endif" href="{{ route('member') }}">Nos membres</a>
				</div>
			</div>
		</div>
		<div class="col-lg-2 col-md-nav-12">
			<a href="javascript:void(0);" class="nav-link @if(Request::url() == route('actualite') || Request::url() == route('agenda') || Request::url() == route('presse')) actived @endif" onclick="$('#actu-list').toggleClass('collapse');">Actualités</a>
			<div id="actu-list" class="collapse">
				<div class="w-100">
					<a class="nav-link sub @if(Request::url() == route('agenda')) actived @endif" href="{{ route('agenda') }}">Agenda</a>
				</div>
				<div class="w-100">
					<a class="nav-link sub @if(Request::url() == route('actualite')) actived @endif" href="{{ route('actualite') }}">Nos événements</a>
				</div>
				<div class="w-100">
					<a class="nav-link sub @if(Request::url() == route('presse')) actived @endif" href="{{ route('presse') }}">La presse en parle</a>
				</div>
			</div>
		</div>
		<div class="col-lg-2 col-md-nav-12">
			<a class="nav-link @if(Request::url() == route('galerie')) actived @endif" href="{{ route('galerie') }}">Galerie photos</a>
		</div>
		<div class="col-lg-2 col-md-nav-12">
			<a class="nav-link @if(Request::url() == route('soutien')) actived @endif" href="{{ route('soutien') }}">Nos soutiens</a>
		</div>
		<div class="col-lg-3 col-md-nav-12">
			<a class="nav-link @if(Request::url() == route('contact')) actived @endif" href="{{ route('contact') }}">Contact/Nous aider</a>
		</div>
	</div>
</nav>
