<div class="connect-pos">
	<div class="connect-pos boxy">
		@if(!session()->has("user"))
		<a class="connect-link" onclick="toggle('connexion')"><span class="v-align-text">Connexion</span><img src="/system/lock.svg" class="cadena"></a>
		<div id="connexion" style="display:none">
			<form action="{{ route('connexion') }}" method="POST">
				{{ csrf_field() }}
				<input name="email" type="email" placeholder="Adresse mail">
				<input name="password" type="password" placeholder="Mot de passe">
				<button class="btn btn-success w-100">Connexion</button>
			</form>
		</div>
		@else
		<a class="connect-link" href="{{ route('utilisateur.compte') }}">Compte</a>
		<a class="connect-link" href="{{ route('logout') }}">Déconnexion</a>
		@endif
	</div>
</div>