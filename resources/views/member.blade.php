@extends('_includes.template')
@section('title')Nos membres @endsection
@section('content')
<div>
	<div class="cdl-title-v2">
		<h1>Découvrez nos membres</h1>
	</div>
	<div class="row jc-center">
		@foreach($pictures as $picture)
		<div class="col-4 ta-center marg-blo t-center img_responsive_size_change">
			<img class="img-fluid" src="/pictures/{{ $picture->name }}.png" alt="{{ $picture->description }}"><h2 class="image-title">{{ $picture->description }}</h2>
		</div>
		@endforeach
	</div>
</div>

<div class="ta-center grey-bg">
	<div class="p-art">
		<h3 class="article-title">Devenir membre</h3>
		<div class="w-100 ta-center">
			<a href="javascript:void(0);" onclick="toggle('new_member')" class="btn see-more">Envie de nous rejoindre ?</a>
		</div>
	</div>
	
	<textarea id="member-zone" hidden>
		{{ $join->content }}
	</textarea>
	<div class="p-art no-upper" id="new_member" style="display: none"></div>
	<script>
		preview("member-zone", "new_member")
	</script>
</div>
@endsection
