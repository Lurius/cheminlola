@extends('_includes.template')
@section('content')
<div class="w-100">
<button class="btn btn-secondary" onclick="toggle('new_user')">+ Ajouter un utilisateur</button>
<div class="box" id="new_user" style="display:none">
	<form action="{{ route('utilisateur.store') }}" method="POST">
		{{ csrf_field() }}
		<div>
			<label>Addresse mail :</label>
		</div>
		<div>
			<input name="email" type="email">
		</div>
		<div>
			<label>Nom : </label>
		</div>
		<div>
			<input name="name" type="text">
		</div>
		<div>
			<label>Mot de passe :</label>
		</div>
		<div>
			<input name="password" type="password">
		</div>
		<button class="w-100 btn btn-success" type="submit">Créer</button>
	</form>
</div>

<table class="w-100 table table-dark">
	<thead>
		<tr>
			<th>Mails</th>
			<th>Permissions</th>
			<th>Action</th>
		</tr>
	</thead>
	<tbody style="text-transform:none;">
		@foreach ($users as $user)
		<tr>
			<td>{{$user->email}}</td>
			<td>
				<form action="{{ route('perm.user', ['id' => $user->id]) }}" method="POST" style="display:contents!important">
					{{ csrf_field() }}
					<ul>
						@foreach($perms as $perm)
						<li>
							<?php $active = false ?>
							@foreach($user->user_rights as $right)
								@if($right->permission_id == $perm->id)
									<?php $active = true ?>
								@endif
							@endforeach

							<label>{{ $perm->description }} :</label>
							<input type="checkbox" name="{{$perm->name}}" value="true" @if($active) checked @endif>
						</li>
						@endforeach
					</ul>
				</td>
				<td>
					<button class="w-100 btn btn-success" type="submit" style="margin-bottom:1em">Enregistrer les permissions</button>
				</form>
				<a href="{{ route('utilisateur.show', ['id' => $user->id]) }}" class="w-100 btn btn-primary" style="margin-bottom:0.5em">Modifier l'utilisateur</a>
				<form action="{{ route('utilisateur.delete', ['id' => $user->id]) }}" method="POST">
					{{ csrf_field() }}
					<button class="btn btn-danger w-100" type="submit">Supprimer l'utilisateur</button>
				</form>
			</td>
		</tr>
		@endforeach
	</tbody>
</table>
@endsection