@extends('_includes.template')
@section('content')

@if(isset($is_admin) && $is_admin)
<a href="{{ route('utilisateur.index') }}" class="btn btn-secondary">Retour</a>
@endif

<div class="w-100">
    <div class="box">
        @if(Request::url() == route('utilisateur.compte'))
        <h2>Mon Compte :</h2>
        @else
        <h2>Compte de {{ $user->name }} :</h2>
        @endif
        <div>
            <ul>
                <li>Adresse mail : {{ $user->email }}</li>
                <li>Nom : {{ $user->name }}</li>
            </ul>
        </div>
        <div>
            <button onclick="toggle('modif_user')" class="w-100 btn btn-success">Modifier</button>
            <div id="modif_user" style="display:none" class="w-100">
                <form action="{{ route('utilisateur.update', ['id' => $user->id]) }}" method="POST">
                    {{ csrf_field() }}
                    <div>
                        <label for="current_email">Email</label>
                    </div>
                    <div>
                        <input class="w-100" type="email" name="current_email" id="current_email" value="{{ $user->email }}">
                    </div>
                    <div>
                        <label for="current_name">Nom</label>
                    </div>
                    <div>
                        <input class="w-100" type="text" name="current_name" id="current_name" value="{{ $user->name }}">
                    </div>
                    @if(Request::url() == route('utilisateur.compte'))
                    <div>
                        <label class="w-100" for="current_pass">Mot de passe actuel</label>
                    </div>
                    <div>
                        <input class="w-100" type="password" name="current_pass" id="current_pass">
                    </div>
                    @else
                    <div>
                        <input hidden type="text" name="is_admin" value="true">
                    </div>
                    @endif
                    <div>
                        <label for="new_pass">Nouveau mot de passe</label>
                    </div>
                    <div>
                        <input class="w-100" type="password" name="new_pass" id="new_pass">
                    </div>
                    <div>
                        <label for="new_pass_redo">Nouveau mot de passe</label>
                    </div>
                    <div>
                        <input class="w-100" type="password" name="new_pass_redo" id="new_pass_redo">
                    </div>

                    <button class="w-100 btn btn-secondary" type="submit">Enregistrer</button>
                </form>
            </div>
        </div>
    </div>
</div>


@endsection
