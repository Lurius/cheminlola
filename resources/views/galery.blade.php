@extends('_includes.template')
@section('title')Galerie @endsection
@section('content')
<div>
	<div class="cdl-title-v2">
		<h1>Galerie photos</h1>
	</div>
	<div class="row jc-center">
		@foreach($pictures as $picture)
		<div class="col-5 marg-bloc t-center img_responsive_size_change">
			<img class="img-fluid" src="/pictures/{{ $picture->name }}.png" alt="{{ $picture->description }}"><h2 class="image-title">{{ $picture->description }}</h2>
		</div>
		@endforeach
	</div>
</div>
@endsection
