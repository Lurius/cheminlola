@extends('_includes.template')
@section('content')
<div class="row">
	<div class="cdl-title-v2">
		<h1>La presse en parle</h1>
		@if($admin)
		<div class="button-margin">
			<a class="btn btn-secondary w-100" href="{{ route('article.create') }}" style="margin-bottom:1em;">+ Ajouter un article de presse</a>
		</div>
		@endif
	</div>
	
	<div class="col-12">
		<div class="box" style="border:0">
			{{-- Recherche --}}
			<form>
				<div class="form-group">
					<input name="search" class="w-100 btn btn-outline">
					<button type="submit" class="w-100 btn btn-secondary">Rechercher</button>
				</div>
			</form>
		</div>

		<section class="w-100">
			@foreach($posts as $post)
			<div class="row grey-bg p-art marg-bloc-extra">
				<div class="col-md-5 col-sm-10 m-auto">
					<img class="cdl-picture" src="/pictures/{{ $post->picture }}.png">
				</div>
				<div class="col-md-6 col-sm-10 ta-justify marg-bloc">
					{{-- <h2 class="cdl-title">{{ $post->title }}</h2> --}}
					<p class="text-article no-upper">{{ $post->resume }}</p>
				</div>
				<div class="col-12 see-more-container">
					<a href="{{ route('article', ['url_title'=>$post->url_title]) }}" class="see-more">Voir plus</a>
				</div>
				@if($admin)
				<div class="row col-12">
					<a href="{{ route('article.edit',['url_title'=>$post->url_title]) }}" class="btn btn-secondary w-100 col-6">Modifier</a>
					<form class="col-6 w-100" action="{{ route('article.delete',['url_title'=>$post->url_title]) }}" method="POST">
						{{ csrf_field() }}
						<button type="submit" class="btn btn-danger w-100">Supprimer</button>
					</form>
				</div>
				@endif
			</div>
			@endforeach
		</section>
	</div>
</div>
@endsection