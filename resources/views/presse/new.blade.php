@extends('_includes.template')
@section('content')

@include('_includes.writing_help')

<form class="box no-upper w-90 grey-bg" action="{{ route('presse.store') }}" method="POST">
	{{ csrf_field() }}
	<input class="d-none" id="type" name="type" value="1">
	<div class="form-title">
		<label for="picture">Nom de l'image de l'article : </label>
	</div>
	<div>
		<input class="w-100" id="picture" name="picture" placeholder="monde_luni_1_janvier_2000">
	</div>
	<div>
		<details>Image visible à côté sur la liste des articles de presse</details>
	</div>

	<div class="form-title">
		<label for="title">Nom du journal et date de publication :</label>
	</div>
	<div>
		<input class="w-100" id="title" name="title" placeholder="Le Monde - Lundi 1 Janvier 2000">
	</div>
	
	<div class="form-title">
		<label for="resume">Resumé :</label>
	</div>
	<div>
		<textarea class="textarea" id="resume" name="resume" onkeypress="preview('resume', 'in_text_resume')"></textarea>
	</div>
	<div>
		<details>Pour Google et la page de recherche interne mais il sera intégré à l'article</details>
	</div>
	<div id="in_text_resume" class="wrap">

	</div>
	
	<div class="form-title">
		<label for="article">Article complet :</label>
	</div>
	<div>
		<textarea class="textarea" id="article" name="article" onkeypress="preview('article', 'in_text')"></textarea>
	</div>
	<div id="in_text" class="wrap">

	</div>
	<button class="w-100 btn btn-secondary" type="submit">Ajouter</button>
	
</form>
@endsection