@extends('_includes.template')
@section('title'){{ $post->title}} @endsection
@section('description'){{ $post->resume }}@endsection

@section('content')
@if($admin)
<a href="{{ route('presse.edit',['url_title'=>$post->url_title]) }}" class="btn btn-secondary">Modifier</a>
<a href="{{ route('presse.delete',['url_title'=>$post->url_title]) }}" class="btn btn-danger">Supprimer</a>
@endif
<div class="w-100 ta-center">
    <img class="cdl-picture" src="/pictures/{{ $post->picture }}.png">
</div>
<article class="no-upper grey-bg">
    <h1 class="cdl-title extra-marg wrap bonus-marg">{{ $post->title}}</h1>


    <aside class="bold extra-marg wrap bonus-marg">{{ $post->resume }}</aside>

    <textarea id="article" hidden>{{ $post->article }}</textarea>
    <div id="in_text" class="extra-marg wrap bonus-marg"></div>
    <script>
        preview("article", "in_text")
    </script>
</article>
@endsection