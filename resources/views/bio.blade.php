@extends('_includes.template')
@section('title')Qui sommes-nous ? @endsection
@section('content')
<div class="w-100 no-upper">
	<div class="cdl-title-v2">
		<h1>Qui sommes-nous ?</h1>
	</div>
	<?php $x=1 ?>
    @foreach($info as $part)
    <div class="@if($x>=2) grey-bg @endif">
        <h2 class="cdl-title marg-head">{{ $part->title }}</h2>
        <div class="row jc-center">
            <textarea id="bio{{$x}}" hidden>{{ $part->content }}</textarea>
            @if($x==2)
            <div class="col-md-6 col-sm-10 m-auto">
                <img class="w-100" src="/pictures/carousel1.png">
            </div>
            @endif
            <div class="@if($x==1)col-10 @else col-md-4 col-sm-10 @endif ta-justify marg-bloc" id="text{{$x}}"></div>
            <script>
                preview("bio<?php echo $x; ?>", "text<?php echo $x; ?>")
            </script>
        <?php $x++ ?>
        </div>
    </div>
	@endforeach
</div>

<div class="back-img-member see-more-sec row">
	<h3 class="no-upper see-more-title col-12">Découvrez nos membres</h3>
	<a href="{{ route('member') }}" class="see-more">Voir plus</a>
</div>
@endsection