{{-- Cette page devra contenir : Le carousel en premier
	Un gros titre en rose centrer : "Notre dernière actualité..." avec la dernière actu sur un fond gris, une image à gauche, le titre en rose majuscule indiquant la date et un texte gras noir pour résumé fini par bouton rose : "VOIR PLUS"
	Section avec titre centrer bleu (comme footer #4ce) contenant les derniers logo de soutien ajouter avec même bouton rose "VOIR PLUS" --}}

@extends('_includes.template')
@section('content')
<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
	<div class="carousel-inner">
		<a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
			<span class="carousel-control-prev-icon" aria-hidden="true"></span>
			<span class="sr-only">Précédent</span>
		</a>
		<div class="carousel-item active w-100">
			<img class="d-block carousel-picture" src="/pictures/carousel1.png" alt="First slide">
		</div>
		<div class="carousel-item w-100">
			<img class="d-block carousel-picture" src="/pictures/carousel2.png" alt="Second slide">
		</div>
		<div class="carousel-item w-100">
			<img class="d-block carousel-picture" src="/pictures/carousel3.png" alt="Third slide">
		</div>
		<div class="carousel-item w-100">
			<img class="d-block carousel-picture" src="/pictures/carousel4.png" alt="Third slide">
		</div>
		<a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
			<span class="carousel-control-next-icon" aria-hidden="true"></span>
			<span class="sr-only">Suivant</span>
		</a>
		<ol class="carousel-indicators">
			<li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
			<li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
			<li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
			<li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
		</ol>
	</div>
</div>

@if(isset($last_event))
<div class="no-upper">
	<div class="cdl-center-title-v2">
		<h2>Notre dernière actualité...</h2>
	</div>
	<div class="row grey-bg p-art"> 
		<div class="col-md-6 col-sm-10 m-auto">
			<img class="cdl-picture" src="/pictures/{{ $last_event->picture }}.png">
		</div>
		<div class="col-md-4 col-sm-10 ta-justify marg-bloc">
			<h3 class="cdl-title">{{ $last_event->title }}</h3>
			<p class="text-article">{{ $last_event->resume }}</p>
		</div>
		<div class="col-12 see-more-container">
			<a href="{{ route('actualite') }}" class="see-more">Voir plus</a>
		</div>
	</div>
</div>
@endif
<div class="no-upper">
	<div class="cdl-center-title-v2">
		<h2 class="section-title-alt">Avec le soutien de</h2>
	</div>
	<div class="col-12 row">
		@foreach ($soutiens as $soutien)
		<span class="module col-3">
			<img src="/pictures/{{ $soutien->name }}.png">
		</span>
		@endforeach
		<div class="col-12 see-more-container">
			<a href="{{ route('soutien') }}" class="see-more">Voir plus</a>
		</div>
	</div>
</div>
@endsection
