@extends('_includes.template')
@section('content')

<div class="w-100">
    <div style="text-align: center; color:#fff">
        <p>Pour modifier les images du carousel, allez directement les modifier dans la gestion d'image</p>

    </div>
    @include('_includes.writing_help')
    <?php $x=1 ?>
    @foreach($parameters as $param)
    <form class="box w-90 grey-bg" action="{{ route('admin.update', [$param->id]) }}" method="POST">
        {{ csrf_field() }}
        <h2>{{ $param->section }}:</h2>
        <div class="no-upper form-title">
            <label for="title">Titre :</label>
        </div>
        <div class="no-upper">
            <input class="w-100" id="title" name="title" value="{{ $param->title }}">
        </div>
        <div class="no-upper form-title">
            <label for="content{{$x}}">Contenu :</label>
        </div>
        <div class="no-upper">
            <textarea class="textarea" id="content{{$x}}" name="content" onkeypress="preview('content{{$x}}', 'render{{$x}}')">{{ $param->content }}</textarea>
            <div id="render{{$x}}" class="no-upper wrap">
            </div>
        </div>
        <div>
            <button class="w-100 btn see-more" type="submit">Enregistrer les modification</button>
        </div>
    </form>
    <?php $x++ ?>
    @endforeach
</div>

@endsection