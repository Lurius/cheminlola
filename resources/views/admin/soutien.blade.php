@extends('_includes.template')
@section('content')

<div class="w-100">
	<div class="box">
		<h2>Nouveau soutien:</h2>
		<div>
			<form action="{{ route('soutien_admin.store') }}" method="POST">
				{{ csrf_field() }}
				<label for="name">Nom du soutien :</label>
				<input id="name" name="name">
				<label for="link">Liens du logo :</label>
				<input id="link" name="link">

				<button type="submit" class="btn btn-primary">Ajouter</button>
			</form>
		</div>

		<ul class="list-group" style="background-color:transparent;">
			@foreach($soutiens as $soutien)
			<li class="list-group-item w-100 p-0" style="background-color:transparent;">
				<div class="box box-bigger">
					<form  action="{{ route('soutien_admin.update',[$soutien->id]) }}" method="POST">
						{{ csrf_field() }}
						<div>
							<label for="name">Nom du soutien :</label>
						</div>
						<div>
							<input class="w-100" id="name" name="name" value="{{ $soutien->name }}">
						</div>
						<div>
							<label for="link">Liens du logo :</label>
						</div>
						<div>
							<input class="w-100" id="link" name="link" value="{{ $soutien->logo }}">
						</div>
						<div>
							<button class="w-100 btn btn-secondary" type="submit">Modifier</button>
						</div>
					</form>
					<div>
					<form action="{{ route('soutien_admin.delete',[$soutien->id]) }}" method="POST">
						{{ csrf_field() }}
						<button class="w-100 btn btn-danger" type="submit">Supprimer</button>
					</form>
					</div>
				</div>
			</li>
			@endforeach
		</ul>
	</div>
</div>

@endsection