@extends('_includes.template')
@section('title')Nos soutiens @endsection
@section('content')
<div class="row w-100 m-auto no-upper">
    <div class="cdl-title-v2">
        <h1>Nos soutiens</h1>
    </div>
    <div class="col-12 row">
    @foreach ($soutiens as $soutien)
    <span class="module col-3">
        <img src="/pictures/{{ $soutien->name }}.png">
    </span>
    @endforeach
    </div>
    
    <div class="cdl-center-title-v2">
        <h2>Nos parrains</h2>
    </div>
    <div class="article-container col-12 row">
        @foreach ($parrains as $parrain)
        <div class="col-4 spec-sup">
            <img src="/pictures/{{ $parrain->name }}.png">
        </div>
        @endforeach
    </div>


</div>
@endsection
