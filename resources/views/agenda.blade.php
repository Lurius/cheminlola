@extends('_includes.template')
@section('title')Agenda @endsection
@section('content')
<div class="no-upper">
	<div class="cdl-title-v2">
		<h1>Agenda</h1>
	</div>
	<div class="row article-container">
		<textarea id="agenda" hidden>
			{{ $info->content }}
		</textarea>
		<div class="col-10 spec-sup" id="text"></div>
		<script>
			preview("agenda", "text")
		</script>
	</div>
</div>

<div class="back-img-event see-more-sec row">
	<h3 class="no-upper see-more-title col-12">Découvrez nos derniers évènements</h3>
	<a href="{{ route('actualite') }}" class="see-more">Voir plus</a>
</div>
@endsection