@extends('_includes.template')
@section('content')
<a href="{{ route('actualite') }}" class="btn btn-secondary">Retour</a>

@include('_includes.writing_help')

<form class="box no-upper w-90 grey-bg" action="{{ route('article.update', ['url_title' => $post->url_title]) }}" method="POST">
    {{ csrf_field() }}
    <input class="d-none" id="type" name="type" value="0">
    <div class="form-title">
		<label for="picture">Nom de l'image de l'article : </label>
	</div>
	<div>
        <input class="w-100" id="picture" name="picture" placeholder="luni_1_janvier_2000" value="{{ $post->picture }}">
	</div>
	<div>
		<details>Image visible à côté sur la liste des articles de presse</details>
	</div>

    <div class="form-title">
        <label for="title">Date de l'évènement :</label>
    </div>
    <div>
        <input class="w-100" id="title" name="title" placeholder="Lundi 1 Janvier 2000" value="{{ $post->title }}">
    </div>
    <div class="form-title">    
        <label for="resume">Resumé :</label>
    </div>
    <div>
        <textarea class="textarea" id="resume" name="resume" onkeypress="preview('resume', 'in_text_resume')">{{ $post->resume }}</textarea>
    </div>
    <div>
        <details>Pour Google et la page de recherche interne</details>
    </div>
    <div id="in_text_resume" class="wrap">

	</div>
    
    <div class="form-title">
        <label for="article">Article complet :</label>
    </div>
    <div>
        <textarea class="textarea" id="article" name="article" onkeypress="preview('article', 'in_text')">{{ $post->article }}</textarea>
    </div>

    <div id="in_text" class="wrap">

    </div>
    
    <button class="w-100 btn btn-secondary" type="submit">Enregistrer les modification</button>

</form>
@endsection