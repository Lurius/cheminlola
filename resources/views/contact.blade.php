@extends('_includes.template')
@section('title')Contact @endsection
@section('content')
<div>
	<div class="cdl-title-v2 w-100 mb-2rem">
		<h1>Nous contacter / nous aider</h1>
	</div>
</div>

<div class="no-upper row">
	<?php $x=1 ?>
	@foreach($contact as $part)
	<div @if($x<=2) class="row col-12 jc-center mb-2rem" @else class="grey-bg col-12 mb-2rem" @endif>
		@if($x==1)
		<div class="col-md-2 col-sm-6">
			<img class="logo-contact" src="/system/phone.svg"> 
		</div>
		@elseif($x==2) 
		<div  class="col-md-2 col-sm-6">
			<img class="logo-contact" src="/system/mail.svg">
		</div>
		@endif
		<div @if($x<=2) class="col-md-3 col-sm-6" @else class="col-12" @endif>
			<h2 @if($x<=2)class="contact-element bold"@elseif($x==3)class="cdl-title extra-marg bold"@endif>{{ $part->title }}</h2>

			<textarea id="contact{{$x}}" hidden>{{ $part->content }}</textarea>
			<div @if($x<=2)class="contact-element"@elseif($x==3)class="ta-center"@endif id="text{{$x}}"></div>
			<script>
				preview("contact<?php echo $x; ?>", "text<?php echo $x; ?>")
			</script>
		</div>
	</div>
	<?php $x++ ?>
	@endforeach
</div>
@endsection