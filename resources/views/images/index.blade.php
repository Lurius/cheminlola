@extends('_includes.template')
@section('content')
<form class="no-upper">
	<select name="type" onchange="this.form.submit()">
		<option @if(!isset($_GET["type"]) || $_GET["type"] == "") selected @endif value="">Toutes</option>
		<option @if(isset($_GET["type"]) && $_GET["type"] == "0") selected @endif value="0">Site/Carousel</option>
		<option @if(isset($_GET["type"]) && $_GET["type"] == "1") selected @endif value="1">Photo membres</option>
		<option @if(isset($_GET["type"]) && $_GET["type"] == "2") selected @endif value="2">Photo galerie</option>
		<option @if(isset($_GET["type"]) && $_GET["type"] == "3") selected @endif value="3">Photo soutiens</option>
		<option @if(isset($_GET["type"]) && $_GET["type"] == "4") selected @endif value="4">Photo parrains</option>
	</select>
</form>
<a href=" {{ route('image.create') }} " class="btn btn-success btn-sm float-right"> Add More </a>
<div class="w-100 no-upper">
	@foreach($images as $image)
	<div class="box">
		<div class="">
			<div class="row">
				{{-- <a href="{{ route('image.edit', ['id'=>$image->id]) }}" class="btn btn-primary">Modifier</a> --}}
				<form action="{{ route('image.delete', ['id'=>$image->id]) }}" method="POST">
					{{ csrf_field() }}
					<button type="submit" class="btn btn-danger">Supprimer</button>
				</form>
			</div>
			<a href="{{ route('image.show', ['id'=>$image->id]) }}" style="display:contents; color: inherit;">
			<div class="card-body">
				<h2 class="w-100 cdl-title no-marg">{{$image->name}}</h2>
				<div class="w-100 ta-center">
					<img src="/pictures/{{$image->name}}.png" class="img-fluid img-thumbnails">
				</div>
			</div>
			</a>
		</div>
	</div>
	@endforeach
</div>
@endsection