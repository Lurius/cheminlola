@extends('_includes.template')
@section('content')
<div class="row no-upper">
	{{-- <a href="{{ route('image.edit', ['id'=> $image->id ]) }}" class="btn btn-primary">Modifier</a> --}}
	<form action="{{ route('image.delete', ['id'=> $image->id ]) }}" method="POST">
		{{ csrf_field() }}
		<button type="submit" class="btn btn-danger">Supprimer</button>
	</form>
</div>
<div class="no-upper grey-bg ta-center">
	<h1 class="cdl-title">{{ $image->name }}</h1>
	<p class="bold">{{ $image->description }}</p>
	<img src="/pictures/{{ $image->name }}.png">
</div>

@endsection