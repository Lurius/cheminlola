@extends('_includes.template')
@section('content')
<div class="w-100">
	<a href="{{ route('image.index') }}" class="btn btn-secondary">Retour</a>
	<form class="box" action="{{ route('image.store') }}" method="post" enctype="multipart/form-data">
		{{ csrf_field() }}
		<div>
			<!-- print success message after file upload  -->
		@if(Session::has('success'))
			<div class="alert alert-success">
				{{ Session::get('success') }}
				@php
					Session::forget('success');
				@endphp
			</div>
		@endif

		
		<div class="form-group" {{ $errors->has('filename') ? 'has-error' : '' }}>
				<label for="type">Catégorie de l'image :</label>
				<select class="form-control" name="type" id="type">
					<option value="0">Site/Carousel</option>
					<option value="1">Photo membres</option>
					<option value="2">Photo galerie</option>
					<option value="3">Photo soutiens</option>
					<option value="4">Photo parrains</option>
				</select>
				<label for="name">Nom :</label>
				<input type="text" name="name" id="name" class="form-control">
				<label for="description">Description :</label>
				<input type="text" name="description" id="description" class="form-control">
				<label for="filename">Image :</label>
				<input type="file" name="filename" id="filename" class="form-control">
				<span class="text-danger"> {{ $errors->first('filename') }}</span>
			</div>
		</div>

		<div class="card-footer">
			<div class="form-group">
				<button type="submit" class="btn btn-success btn-md"> Enregistrer </button>
			</div>   
		</div>
	</form>
</div>
@endsection