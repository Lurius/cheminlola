@extends('_includes.template')
@section('title')Mention légale @endsection
@section('content')
<div class="no-upper">
	<textarea id="legal-title" hidden>{{ $info->title }}</textarea>
	<textarea id="legal-content" hidden>{{ $info->content }}</textarea>
	<div class="cdl-title-v2 mb-2rem">
		<h1 id="title"></h1>
	</div>
	<div class="bold ta-center">
		<p id="content"></p>
	</div>
	<script>
		preview("legal-title", "title");
		preview("legal-content", "content")
	</script>
</div>
@endsection